//var exec = require("child_process").exec;


function start(request, response) {
    console.log("Request handler 'start' was called.");
    //exec("ip route get 8.8.8.8 | awk 'NR==1 {print $NF}'", function (error, stdout, stderr) {
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Server is on");
    response.end();
    //});
}
function upload(request, response) {
    console.log("Request handler 'upload' was called.");
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("Upload ready");
    response.end();
}
function login(request, response) {
    console.log("Request handler 'login' was called.");
    console.log("Request: ", request);
    response.writeHead(200, {"Content-Type": "text/plain"});
    response.write("OK");
    response.end();
}


exports.start = start;
exports.upload = upload;
exports.login = login;
