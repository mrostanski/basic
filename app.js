var server = require('./server');
var router = require("./router");
var handlers = require('./handlers');
var config = require('./config');

var handle = {}

handle["/"] = handlers.start;
handle["/login"] = handlers.login;
handle["/upload"] = handlers.upload;

server.start(router.route, handle);
