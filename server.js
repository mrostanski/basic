var http = require('http');
var url = require('url');
var exec = require("child_process").exec;
var config = require('./config');

function start(route, handle) {
    http.createServer(function(req, res) {
        var pathname = url.parse(req.url).pathname;
        console.log("Request for " + pathname + " received.");
        route(handle, pathname, req, res);
    }).listen(3333);
    console.log("Server started on " + process.env.IP + ", port " + config.port);
}

exports.start = start;
